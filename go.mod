module gitlab.com/remotejob/mlfrontendadvanced

go 1.13

require (
	github.com/fsnotify/fsnotify v1.4.7
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/mssola/user_agent v0.5.0
	github.com/oklog/ulid v1.3.1
	github.com/oklog/ulid/v2 v2.0.2 // indirect
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.5.0
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/net v0.0.0-20191126235420-ef20fe5d7933 // indirect
	golang.org/x/sys v0.0.0-20191128015809-6d18c012aee9 // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
