
function clickFunction(redir) {
    console.info("Click desk")
    window.location.href = redir
}

var chatuuid = create_UUID()


const chatux = new ChatUx();

const opt = {

    bot: {
        botPhoto: defaultPhoto.Img_file_name,
        widget: {
            sendLabel: 'SEND',
            placeHolder: 'Kirjoita viesti...'

        }
    },
    api: {
        endpoint: chaturl,
        method: 'POST',
        dataType: 'json',
        params: {
            'nguid': nguid,
            'chatuuid': chatuuid,
            'name': defaultPhoto.Name,
            'age': defaultPhoto.Age,
            'city': defaultPhoto.City,
            'phone': defaultPhoto.Phone
        },
        errorResponse: {
            output: [
                //Message displayed when a network error occurs when accessing the chat server
                { type: 'text', value: 'Sorry, an error occurred' }
            ]
        }
    },
    window: {
        title: defaultPhoto.Name+' '+defaultPhoto.Age+'v '+defaultPhoto.City,
        infoUrl: 'chat/'+defaultPhoto.Id,
        size: {
            width: 350,//window width in px
            height: 500,//window height in px
            minWidth: 300,//window minimum-width in px
            minHeight: 300,//window minimum-height in px
            titleHeight: 80//title bar height in px
        },
        appearance: {
            //border - border style of the window
            border: {
                shadow: '2px 2px 10px  rgba(0, 0, 0, 0.5)',
                width: 0,
                radius: 6
            },
            //titleBar - title style of the window
            titleBar: {
                fontSize: 14,
                color: 'white',
                background: '#4784d4',
                leftMargin: 40,
                height: 40,
                buttonWidth: 36,
                buttonHeight: 16,
                buttonColor: 'white',
                buttons: [
                    //Icon named 'hideButton' to close chat window
                    {
                        fa: 'fas fa-times',//specify font awesome icon
                        name: 'hideButton',
                        visible: true
                    }
                ],
                buttonsOnLeft: [
                    //Icon named 'info' to jump to 'infourl' when clicked
                    {
                        fa: 'fas fa-female fa-2x',//specify font awesome icon
                        name: 'info',
                        visible: true
                    }
                ],
            },
        }

    },

    methods: {

        onUserInput: (userInputText) => {

            console.log('#onUserInput userInputText=' + userInputText);


        },

        onServerResponse: (response) => {

            console.log('#onServerResponse response=' + JSON.stringify(response));
            // return response;
            return { "output": [{ "type": "text", "value": response['Answer'] }] }
        }
    }
};


chatux.init(opt);
chatux.start(true);


function create_UUID() {
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = (dt + Math.random() * 16) % 16 | 0;
      dt = Math.floor(dt / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }
