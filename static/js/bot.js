

var uuid = create_UUID()

var botui = new BotUI('my-botui-app');

function askBotServer(ask, cb) {
  fetch(chaturl, {
    method: 'post',
    headers: {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(askarr)
  }).then(res => res.json())
    .then(function (res) {
      cb(res.Answer);

    })

}


function init() {

  botui.action.text({

    action: {
      placeholder: 'Kirjoita viesti...'
    }

  }).then(function (res) {


    botui.message.bot({
      delay: 5000,
      loading: true
    }).then(function (index) {
      loadingMsgIndex = index
      askarr = { "nguid": nguid, "chatuuid": uuid, "phone": defaultPhoto.Phone, "name": defaultPhoto.Name, "age": defaultPhoto.Age, "city": defaultPhoto.City, "mob": mob, "text": res.value }

      askBotServer(askarr, showAnswer)
    });
  })

}

function showAnswer(answer) {
  botui.message
    .update(loadingMsgIndex, {
      content: answer
    }).then(init)
}



function create_UUID() {
  var dt = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (dt + Math.random() * 16) % 16 | 0;
    dt = Math.floor(dt / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
}

init()


