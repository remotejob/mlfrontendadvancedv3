package config

import (
	"fmt"
	"html/template"
	"log"
	"net/rpc"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

type Constants struct {
	PORT string

	Images struct {
		Url string
	}
	Chat struct {
		Url string
	}

	Rpcserver struct {
		Url string
	}
	Dev struct {
		Active bool
		Nguid  string
		Allowedcountry string
	}
}

type Config struct {
	Constants
	HomepageTpl *template.Template
	HomepageTplDesk *template.Template
	ChatTpl     *template.Template
	ChatTplDesk     *template.Template
	RpcClient   *rpc.Client
}

func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	config.Constants = constants
	if err != nil {
		return &config, err
	}

	homepageTpl := template.Must(template.New("index.html").ParseFiles("templates/index.html"))
	config.HomepageTpl = homepageTpl
	homepageTplDesk := template.Must(template.New("index.html").ParseFiles("templates/indexdesk.html"))
	config.HomepageTplDesk = homepageTplDesk	
	chatTpl := template.Must(template.New("chat.html").ParseFiles("templates/chat.html"))
	config.ChatTpl = chatTpl
	chatTplDesk := template.Must(template.New("chatdesk.html").ParseFiles("templates/chatdesk.html"))
	config.ChatTplDesk = chatTplDesk
	rpcclient,err := rpc.DialHTTP("tcp", config.Constants.Rpcserver.Url)
	if err != nil {
		return &config, err

	}

	config.RpcClient = rpcclient

	return &config, err
}

func initViper() (Constants, error) {
	viper.SetConfigName("mlfrontendadvanced.config") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")                         // Search the root directory for the configuration file
	err := viper.ReadInConfig()                      // Find and read the config file
	if err != nil {                                  // Handle errors reading the config file
		return Constants{}, err
	}
	viper.WatchConfig() // Watch for changes to the configuration file and recompile
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	// viper.SetDefault("PORT", "6000")
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}

	var constants Constants
	err = viper.Unmarshal(&constants)
	return constants, err
}
