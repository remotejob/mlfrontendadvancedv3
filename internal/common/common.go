package common

import (
	"math/rand"
	"time"

	"github.com/oklog/ulid"
)

func Buulid() ([]byte, error) {

	var res []byte

	t := time.Now()
	entropy := ulid.Monotonic(rand.New(rand.NewSource(t.UnixNano())), 0)

	uuid, err := ulid.New(ulid.Timestamp(t), entropy)
	if err != nil {
		return nil, err
	}

	// uuidbin, err := uuid.MarshalBinary()
	uuidbin, err := uuid.MarshalText()
	if err != nil {
		return nil, err
	}

	res = uuidbin

	return res, nil

}
