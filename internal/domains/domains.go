package domains
import (
	"github.com/mssola/user_agent"
)
type Photo struct {
	Id          int
	Name        string
	Age         int
	Phone       string
	City        string
	Imgfilename string `json:"Img_file_name"`

}

type Payload struct {
	Site    string `json: "Site"`
	Guuid   string `json: "Guuid"`
	Mob     string `json: "Mob"`
	Ua      string `json: "Ua"`
	Urlp    string `json: "Urlp"`
	Referer string `json: "Referer"`
}


type Photos struct {
	AllowedCountry string
	Ua *user_agent.UserAgent
	Nguid string
	Defaultphoto Photo
	Allphotos []Photo
	ChatUrl string

}

type Item struct {
	Page []byte

}