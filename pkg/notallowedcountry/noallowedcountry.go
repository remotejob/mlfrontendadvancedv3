package notallowedcountry

import (
	"log"

	"gitlab.com/remotejob/mlfrontendadvanced/internal/config"
	"gitlab.com/remotejob/mlfrontendadvanced/internal/domains"
)

func CreatePage(config *config.Config) []byte {

	var reply domains.Item

	err := config.RpcClient.Call("API.GetDelete", "", &reply)
	if err != nil {

		log.Fatalln(err)

	}

	return reply.Page

}
