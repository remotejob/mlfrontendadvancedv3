package collectorhandler

import (
	"bytes"
	"encoding/json"
	"net/http"

	"gitlab.com/remotejob/mlfrontendadvanced/internal/domains"
)

func Send(site string, guuid string, mob string, ua string, urlp string, referer string) error {

	message := domains.Payload{site, guuid, mob, ua, urlp, referer}

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(message)

	// bytesRepresentation, err := json.Marshal(message)
	// if err != nil {

	// 	return err
	// }
	// resp, err := http.Post("http://10.132.138.30:8080/v0/insert", "application/json", bytes.NewBuffer(bytesRepresentation))
	// if err != nil {
	// 	// log.Println(err)
	// 	return err
	// }

	resp, err := http.Post("http://10.132.138.30:8080/v0/insert", "application/json; charset=utf-8", b)
	if err != nil {
		// log.Println(err)
		return err
	}

	defer resp.Body.Close()

	return nil

}
