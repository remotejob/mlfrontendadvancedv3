package handler

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"gitlab.com/remotejob/mlfrontendadvanced/pkg/collectorhandler"
	"gitlab.com/remotejob/mlfrontendadvanced/pkg/notallowedcountry"

	"gitlab.com/remotejob/mlfrontendadvanced/internal/common"
	"gitlab.com/remotejob/mlfrontendadvanced/internal/domains"
	"gitlab.com/remotejob/mlfrontendadvanced/pkg/allowedcountry"

	"github.com/go-chi/chi"
	"github.com/mssola/user_agent"
	"gitlab.com/remotejob/mlfrontendadvanced/internal/config"
)

type Config struct {
	*config.Config
}

func New(configuration *config.Config) *Config {
	return &Config{configuration}
}

func (config *Config) Routes() *chi.Mux {
	router := chi.NewRouter()

	router.Route("/alive", func(r chi.Router) {

		r.Get("/", config.Alive)
	})

	router.Route("/chat/{id}", func(r chi.Router) {
		r.Use(config.CommonCtx)
		r.Use(config.ChatCtx)
		r.Get("/", config.Chat)
	})

	fs := http.StripPrefix("/static/", http.FileServer(http.Dir("./static")))
	router.Handle("/static/*", fs)

	router.Route("/", func(r chi.Router) {
		r.Use(config.CommonCtx)
		r.Get("/*", config.Index)
	})

	return router
}

func (config *Config) Alive(w http.ResponseWriter, r *http.Request) {

	w.Header().Add("Alive", "OK")

	w.Write([]byte("OK"))
}

func (config *Config) render(w http.ResponseWriter, r *http.Request, tpl *template.Template, name string, data interface{}) {

	buf := new(bytes.Buffer)
	if err := tpl.ExecuteTemplate(buf, name, data); err != nil {
		fmt.Printf("\nRender Error: %v\n", err)
		return
	}
	w.Write(buf.Bytes())
}

func (config *Config) CommonCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		var photos domains.Photos
		ua := user_agent.New(r.UserAgent())

		allowedcountryvar := r.Header.Get("X-Allowed-Contry")

		if config.Constants.Dev.Active {

			allowedcountryvar = config.Constants.Dev.Allowedcountry

		}

		if allowedcountryvar == "yes" {

			site := r.URL.Host
			if site == "" {
				site = r.Host
			}

			rphotos, err := allowedcountry.CreatePage(config.Config, "http://104.131.122.192:3001/display?op=thumbnail&w=200&h=300&path=")
			if err != nil {

				log.Fatalln(err)
			}
			var nguid string

			rn := rand.New(rand.NewSource(time.Now().UnixNano()))
			rnum := rn.Intn(len(rphotos))
			photos = domains.Photos{allowedcountryvar, ua, nguid, rphotos[rnum], rphotos, config.Constants.Chat.Url}

			var mob string
			if ua.Mobile() {
				mob = "yes"
			} else {
				mob = "no"
			}
			urlp := r.URL.Path

			brname, _ := ua.Browser()

			referer := r.Header.Get("Referer")
			log.Println("mob", mob, "url", urlp, "ref", referer)

			if urlp == "/" || strings.HasSuffix(urlp, ".html") {

				if site != "" && brname != "" {

					if config.Dev.Active {

						nguid = config.Dev.Nguid

					} else {

						cookie, err := r.Cookie("gouuid")
						if err != nil {
							cuuid, err := common.Buulid()
							if err != nil {
								log.Println(err)

							}
							log.Println("set cookie")
							expiration := time.Now().Add(365 * 24 * time.Hour)
							cookie := http.Cookie{Name: "gouuid", Value: string(cuuid), Expires: expiration}

							http.SetCookie(w, &cookie)
							nguid = string(cuuid)

						} else {

							nguid = cookie.Value
						}

					}

					err = collectorhandler.Send(site, nguid, mob, brname, urlp, referer)
					if err != nil {
						log.Println(err)
					}
				}
			}

		} else {
			photos = domains.Photos{
				Ua: ua,
			}

		}

		ctx := context.WithValue(r.Context(), "photos", photos)
		next.ServeHTTP(w, r.WithContext(ctx))
	})

}

func (config *Config) ChatCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		ctx := r.Context()

		photos, ok := ctx.Value("photos").(domains.Photos)
		if !ok {
			http.Error(w, http.StatusText(422), 422)
			return
		}

		id := chi.URLParam(r, "id")

		qurl := config.Constants.Images.Url + "/" + id
		req, err := http.NewRequest("GET", qurl, nil)
		if err != nil {
			log.Fatal(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		var photo domains.Photo

		err = json.Unmarshal(body, &photo)
		if err != nil {
			log.Fatalln(err.Error())
		}

		baseUrl := "http://104.131.122.192:3001/display?op=thumbnail&w=200&h=300&path="

		imgurl := baseUrl + strconv.Itoa(photo.Id) + "/original/" + url.QueryEscape(photo.Imgfilename)

		photo.Imgfilename = imgurl

		photos.Defaultphoto = photo

		ctx = context.WithValue(r.Context(), "photos", photos)
		next.ServeHTTP(w, r.WithContext(ctx))

	})

}

func (config *Config) Chat(w http.ResponseWriter, r *http.Request) {

	ctx := r.Context()

	photos, ok := ctx.Value("photos").(domains.Photos)
	if !ok {
		http.Error(w, http.StatusText(422), 422)
		return
	} else {

	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	if photos.Ua.Mobile() {
		config.render(w, r, config.ChatTpl, "chat.html", photos)
	} else {
		config.render(w, r, config.ChatTplDesk, "chatdesk.html", photos)
	}

}

func (config *Config) Index(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	ctx := r.Context()

	photos, ok := ctx.Value("photos").(domains.Photos)
	if !ok {
		http.Error(w, http.StatusText(422), 422)
		return
	} else {

		if photos.AllowedCountry == "yes" {

			http.Redirect(w, r, "https://sexclub.fi:443", http.StatusMovedPermanently)

			// var url *url.URL
			// if photos.Ua.Mobile() {
			// 	// url, _ = url.Parse("http://chatfree.fi")
			// 	url, _ = url.Parse("https://chatfi.fi")

			// 	// proxy.ServeHTTP(w, r)

			// } else {
			// 	// url, _ = url.Parse("http://freechat.fi")
			// 	url, _ = url.Parse("https://chatfi.fi")
			// 	// config.render(w, r, config.HomepageTplDesk, "indexdesk.html", photos)

			// }
			// proxy := httputil.NewSingleHostReverseProxy(url)
			// // r.URL.Host = url.Host
			// // r.URL.Scheme = url.Scheme
			// // r.Host = url.Host
			// r.URL.Host = "chatfi.fi"
			// r.URL.Scheme = "https"
			// r.Host = "chatfi.fi"

			// proxy.ServeHTTP(w, r)

		} else {

			if photos.Ua.Bot() {

				name, _ := photos.Ua.Browser()
				// log.Println(name, version)

				url := r.URL.Path

				if url == "/" || strings.HasSuffix(url, ".html") {

					if name == "Googlebot" || name == "bingbot" {

						w.Write(notallowedcountry.CreatePage(config.Config))

					} else {

						w.WriteHeader(http.StatusNotFound)
					}

				} else {

					w.WriteHeader(http.StatusNotFound)

				}
			} else {

				w.WriteHeader(http.StatusNotFound)

			}

		}

	}

}
