package allowedcountry

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"

	"gitlab.com/remotejob/mlfrontendadvanced/internal/config"
	"gitlab.com/remotejob/mlfrontendadvanced/internal/domains"
)

func CreatePage(config *config.Config, baseUrl string) ([]domains.Photo, error) {

	req, err := http.NewRequest("GET", config.Constants.Images.Url, nil)
	if err != nil {

		return nil, err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {

		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {

		return nil, err
	}
	var photos []domains.Photo

	err = json.Unmarshal(body, &photos)
	if err != nil {

		return nil, err
	}

	for i, photo := range photos {

		imgurl := baseUrl + strconv.Itoa(photo.Id) + "/original/" + url.QueryEscape(photo.Imgfilename)

		photos[i].Imgfilename = imgurl

	}
	return photos, nil
}
